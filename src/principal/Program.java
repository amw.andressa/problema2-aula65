package principal;

import entities.Product;

import java.util.Locale;
import java.util.Scanner;

public class Program {

    public static void main(String[] args) {

        Locale.setDefault(Locale.US);
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter product data: ");

        Product product = new Product();
        product.setName("TV");
        product.setPrice(900.00);
        product.setQuantity(10);

        double total = product.totalValueInStock();

        System.out.println("Product data: "
                +product.getName()
                + ", "
                + product.getPrice()
                + ", " + product.getQuantity()
                + " units, Total: $" + total);

        System.out.println("Enter the number of products to be added in stock: ");

        int quantity = sc.nextInt();
        product.addProduct(quantity);
        System.out.println("Updated data: " +product.getName() + ", $ "
                + product.getPrice() + ", "
                + product.getQuantity() + " units, Total: $ "
                + product.totalValueInStock());

        System.out.println("Enter the number of products to be removed from stock: ");
        quantity = sc.nextInt();
        product.removeProduct(quantity);
        System.out.println("Updated data: "
                + product.getName() + ", "
                + product.getPrice() + ", "
                + product.getQuantity() + "units, Total $ "
                + product.totalValueInStock());
    }
}
